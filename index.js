exports.randomizeInitialSetup = function() {
	const cols = Math.floor(Math.random() * 49) + 4;
	const lines = Math.floor(Math.random() * 49) + 4;
	const startData = [];
	const initialNumberOfLiveCells = cols * lines / 2;
	for (let i = 0; i < initialNumberOfLiveCells; i++) {
		const x = Math.floor(Math.random() * cols);
		const y = Math.floor(Math.random() * lines);
		startData.push({x, y});
	}
	const result = {
		cols,
		lines,
		startData,
	};
	return result;
}

exports.setupLife = function(setup) {
	if (!setup || typeof(setup) !== 'object') {
		throw 'setup not defined or not an object';
	}
	if (!setup.cols || typeof(setup.cols) !== 'number') {
		throw 'cols not defined or not a number';
	}
	if (!setup.lines || typeof(setup.lines) !== 'number') {
		throw 'lines not defined or not a number';
	}
	if (!setup.startData || !Array.isArray(setup.startData)) {
		throw 'startData not defined or not an array';
	}
	const board = [];
	for (let i = 0; i < setup.lines; i++) {
		board[i] = [];
		for (let j = 0; j < setup.cols; j++) {
			board[i][j] = ' ';
		}
	}
	setup.startData.forEach(data => {
		if (data.x === undefined || typeof(data.x) !== 'number' || data.y === undefined || typeof(data.y) !== 'number') {
			throw JSON.stringify(data)+' mal defined';
		}
		if (data.x >= setup.cols || data.x < 0) {
			throw JSON.stringify(data)+' mal defined, x cannot be '+data.x;
		}
		if (data.y >= setup.lines || data.y < 0) {
			throw JSON.stringify(data)+' mal defined, y cannot be '+data.y;
		}
		board[data.y][data.x] = 'X';
	});
	return board;
}

function printBoard(board) {
	for (let i = 0; i < board.length; i++) {
		process.stdout.cursorTo(0,-board.length+i);
		process.stdout.clearLine();
	}
	process.stdout.cursorTo(0);
	let prettyBoard = '';
	for (let i = 0; i < board.length; i++) {
		let line = '';
		for (let j = 0; j < board[i].length; j++) {
			line += (j === 0 ? '|' : '') + board[i][j] + (j !== board[i].length ? '|' : '');
		}
		prettyBoard += `${i === 0 ? '' : '\n'}${i < 10 ? '0' + i : i}${line}`;
	}
	process.stdout.write(`${prettyBoard}${"\033[0G"}\n`); 
}

function cloneBoard(board) {
	const newBoard = [];
	for (let i = 0; i < board.length; i++) {
		newBoard[i] = board[i].slice(0);
	}
	return newBoard;
}

function getNumNeighbors(lastBoard, i, j) {
	let count = 0;
	if (lastBoard[i-1] && lastBoard[i-1][j-1] === 'X') {
		count++;
	}
	if (lastBoard[i-1] && lastBoard[i-1][j] === 'X') {
		count++;
	}
	if (lastBoard[i-1] && lastBoard[i-1][j+1] === 'X') {
		count++;
	}
	if (lastBoard[i][j-1] === 'X') {
		count++;
	}
	if (lastBoard[i][j+1] === 'X') {
		count++;
	}
	if (lastBoard[i+1] && lastBoard[i+1][j-1] === 'X') {
		count++;
	}
	if (lastBoard[i+1] && lastBoard[i+1][j] === 'X') {
		count++;
	}
	if (lastBoard[i+1] && lastBoard[i+1][j+1] === 'X') {
		count++;
	}
	return count;
}

function updateBoard(lastBoard, currentBoard) {
	for (let i = 0; i < lastBoard.length; i++) {
		for (let j = 0; j < lastBoard[i].length; j++) {
			const numNeighbors = getNumNeighbors(lastBoard, i, j);
			if (lastBoard[i][j] === 'X') {
				if (numNeighbors < 2 || numNeighbors > 3) {
					currentBoard[i][j] = ' ';
				}
			} else {
				if (numNeighbors === 3) {
					currentBoard[i][j] = 'X';
				}
			}
		}
	}
	return currentBoard;
}

function compareBoards(lastBoard, currentBoard) {
	for (let i = 0; i < lastBoard.length; i++) {
		for (let j = 0; j < lastBoard[i].length; j++) {
			if (lastBoard[i][j] !== currentBoard[i][j]) {
				return true;
			}
		}
	}
	return false;
}

function keepSimulating(lastBoard, currentBoard) {
	setTimeout(() => {
		printBoard(currentBoard);
		currentBoard = updateBoard(lastBoard, currentBoard);
		const isValid = compareBoards(lastBoard, currentBoard);
		lastBoard = cloneBoard(currentBoard);
		if (isValid) {
			keepSimulating(lastBoard, currentBoard);
		}
	}, 500);
}

exports.simulateLife = function(board) {
	const lastBoard = cloneBoard(board);
	const currentBoard = cloneBoard(board);
	keepSimulating(lastBoard, currentBoard);
}
