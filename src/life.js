const index = require('../index');

async function play() {
	try{
		const initialSetup = index.randomizeInitialSetup();
		const board = await index.setupLife(initialSetup);
		await index.simulateLife(board);
	} catch (error) {
		console.warn(error);
	}
}

play();